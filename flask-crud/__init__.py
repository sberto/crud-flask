from flask import Flask, Blueprint
from flask_sqlalchemy import SQLAlchemy
import os

db = SQLAlchemy()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///notes.db'

    db.init_app(app)

    from .models import Notes

    with app.app_context():
        db.create_all()  # Create sql tables for our data models

    from . import note
    app.register_blueprint(note.bp)
    app.add_url_rule('/', endpoint='index')
    
    @app.route('/hi')
    def hello_world():
        return 'Hello, World!'

    # my 404 error
    @app.errorhandler(404)
    def page_not_found(e):
        # note that we set the 404 status explicitly
        return '<center><h1>Mi dispiace ma hai cannato l\'url!</h1><br><img src="https://us.123rf.com/450wm/dimarik16/dimarik161801/dimarik16180100397/94571459-sad-beagle-dog-portrait-tired-beagle-dog-close-up-.jpg?ver=6"><br><br>' + str(e)

    return app