from . import db

class Notes(db.Model):
    __tablename__ = 'texts'
    
    id = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.String(200), nullable=False)

    def __repr__(self):
        return '<Task %r>' % self.id
