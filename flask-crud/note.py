from flask import Flask,redirect,url_for,render_template,request, Blueprint, current_app
from flask_sqlalchemy import SQLAlchemy
from .models import Notes, db
from .dog_list import dog_list
import random

bp = Blueprint("note", __name__)

@bp.route('/',methods=['GET','POST'])
def home():
    if request.method=='POST':
        received_note = request.form['note']
        new_note = Notes(note = received_note)

        try:
            db.session.add(new_note)
            db.session.commit()
            return redirect(url_for('index'))
        except:
            return 'Bug adding data to database'

    list = Notes.query.all()
    img = []
    for el in list:
        img.append(random.choice(dog_list))
    return render_template('index.html', list=list, img=img)

if __name__ == '__main__':
    #DEBUG is SET to TRUE. CHANGE FOR PROD
    app.run(port=5000,debug=True)

@bp.route('/<int:id>/update', methods=['GET','POST'])
def update(id):
    el = Notes.query.get_or_404(id)

    if request.method=='POST':
        el.note = request.form['note']

        try:
            db.session.commit()
        except:
            return 'Bug adding data to database'
        
        return redirect(url_for('index'))
    
    return render_template('update.html', el=el)

@bp.route('/<int:id>/delete', methods=['POST'])
def delete(id):
    try:
        Notes.query.filter_by(id=id).delete()
        db.session.commit()
    except:
        return 'Bug removing data to database'

    return redirect(url_for('index'))

@bp.route('/staccah')
def delete_everything():
    Notes.query.delete()
    db.session.commit()
    return redirect(url_for('index'))